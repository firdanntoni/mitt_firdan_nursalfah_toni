﻿using Mitt.Commons.BaseModel;
using Mitt.Commons.ViewModels;
using Mitt.Data.Context;
using Mitt.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Mitt.Data.Entities;

namespace Mitt.Repository.Implementations
{
    public class SkillModelAccess : ISkillModelAccess
    {
        private readonly MittContext _context;

        public SkillModelAccess(MittContext context)
        {
            _context = context;
        }

        public async Task<ResponseDto<List<SkillViewModel>>> GetAll()
        {
            try
            {
                List<SkillViewModel> data = await (from a in _context.Skills
                                                              select new SkillViewModel
                                                              {
                                                                  SkillId = a.SkillId,
                                                                  SkillName = a.SkillName,
                                                                  CreatedBy = a.CreatedBy,
                                                                  CreatedOn = a.CreatedOn,
                                                                  ModifiedBy = a.ModifiedBy,
                                                                  ModifiedOn = a.ModifiedOn,
                                                                  DeletedBy = a.DeletedBy,
                                                                  DeletedOn = a.DeletedOn,
                                                                  IsDelete = a.IsDelete


                                                              }).ToListAsync();

                return new ResponseDto<List<SkillViewModel>>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<List<SkillViewModel>>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillViewModel>> GetById(int id)
        {
            try
            {
                SkillViewModel data = await (from a in _context.Skills
                                             where a.SkillId == id
                                                        select new SkillViewModel
                                                        {
                                                            SkillId = a.SkillId,
                                                            SkillName = a.SkillName,
                                                            IsDelete = a.IsDelete,
                                                            CreatedBy = a.CreatedBy,
                                                            CreatedOn = a.CreatedOn,
                                                            ModifiedBy = a.ModifiedBy,
                                                            ModifiedOn = a.ModifiedOn,
                                                            DeletedBy = a.DeletedBy,
                                                            DeletedOn = a.DeletedOn
                                                        }).FirstOrDefaultAsync();
                return new ResponseDto<SkillViewModel>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillViewModel>> PostCreate(SkillViewModel data)
        {
            try
            {
                Skill a = new Skill
                {
                    SkillId = data.SkillId,
                    SkillName = data.SkillName,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = data.ModifiedBy,
                    ModifiedOn = data.ModifiedOn,
                    DeletedBy = data.DeletedBy,
                    DeletedOn = data.DeletedOn,
                    IsDelete = data.IsDelete
                };
                _context.Skills.Add(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>();

            }
            catch (Exception ex)
            {

                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }

        }

        public async Task<ResponseDto<SkillViewModel>> PostEdit(SkillViewModel data)
        {
            try
            {
                Skill a = new Skill
                {
                    SkillId = data.SkillId,
                    SkillName = data.SkillName,
                    CreatedBy = data.CreatedBy,
                    CreatedOn = data.CreatedOn,
                    ModifiedBy = data.ModifiedBy,
                    ModifiedOn = data.ModifiedOn,
                    DeletedBy = data.DeletedBy,
                    DeletedOn = data.DeletedOn,
                    IsDelete = data.IsDelete
                };
                _context.Update(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>();

            }
            catch (Exception ex)
            {

                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillViewModel>> PostDelete(SkillViewModel model)
        {
            try
            {
                Skill data = new Skill
                {
                    SkillId = model.SkillId,
                    SkillName = model.SkillName,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                    ModifiedBy = model.ModifiedBy,
                    ModifiedOn = model.ModifiedOn,
                    DeletedBy = model.DeletedBy,
                    DeletedOn = model.DeletedOn,
                    IsDelete = model.IsDelete

                };
                _context.Update(data);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillViewModel>(true, "Success insert data!");
            }
            catch (Exception ex)
            {

                return new ResponseDto<SkillViewModel>(false, ex.Message);
            }
        }
    }
}
