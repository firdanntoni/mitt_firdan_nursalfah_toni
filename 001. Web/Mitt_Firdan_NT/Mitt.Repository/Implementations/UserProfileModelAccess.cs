﻿using Mitt.Commons.BaseModel;
using Mitt.Commons.ViewModels;
using Mitt.Data.Context;
using Mitt.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Mitt.Data.Entities;

namespace Mitt.Repository.Implementations
{
    public class UserProfileModelAccess : IUserProfile
    {
        private readonly MittContext _context;

        public UserProfileModelAccess(MittContext context)
        {
            _context = context;
        }

        public async Task<ResponseDto<List<UserProfileViewModel>>> GetAll()
        {
            try
            {
                List<UserProfileViewModel> data = await (from a in _context.UserProfiles
                                                              select new UserProfileViewModel
                                                              {
                                                                  IdUserProfile = a.IdUserProfile,
                                                                  Username = a.Username,
                                                                  Name = a.Name,
                                                                  Address = a.Address,
                                                                  Bod = a.Bod,
                                                                  Email = a.Email,
                                                                  CreatedBy = a.CreatedBy,
                                                                  CreatedOn = a.CreatedOn,
                                                                  ModifiedBy = a.ModifiedBy,
                                                                  ModifiedOn = a.ModifiedOn,
                                                                  DeletedBy = a.DeletedBy,
                                                                  DeletedOn = a.DeletedOn,
                                                                  IsDelete = a.IsDelete


                                                              }).ToListAsync();

                return new ResponseDto<List<UserProfileViewModel>>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<List<UserProfileViewModel>>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<UserProfileViewModel>> GetById(int id)
        {
            try
            {
                UserProfileViewModel data = await (from a in _context.UserProfiles
                                                        where a.IdUserProfile == id
                                                        select new UserProfileViewModel
                                                        {
                                                            IdUserProfile = a.IdUserProfile,
                                                            Username = a.Username,
                                                            Name = a.Name,
                                                            Address = a.Address,
                                                            Bod = a.Bod,
                                                            Email = a.Email,
                                                            CreatedBy = a.CreatedBy,
                                                            CreatedOn = a.CreatedOn,
                                                            ModifiedBy = a.ModifiedBy,
                                                            ModifiedOn = a.ModifiedOn,
                                                            DeletedBy = a.DeletedBy,
                                                            DeletedOn = a.DeletedOn,
                                                            IsDelete = a.IsDelete
                                                        }).FirstOrDefaultAsync();
                return new ResponseDto<UserProfileViewModel>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<UserProfileViewModel>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<UserProfileViewModel>> PostCreate(UserProfileViewModel data)
        {
            try
            {
                UserProfile a = new UserProfile
                {
                    IdUserProfile = data.IdUserProfile,
                    Username = data.Username,
                    Name = data.Name,
                    Address = data.Address,
                    Bod = data.Bod,
                    Email = data.Email,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = data.ModifiedBy,
                    ModifiedOn = data.ModifiedOn,
                    DeletedBy = data.DeletedBy,
                    DeletedOn = data.DeletedOn,
                    IsDelete = data.IsDelete
                };
                _context.UserProfiles.Add(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<UserProfileViewModel>();

            }
            catch (Exception ex)
            {

                return new ResponseDto<UserProfileViewModel>(false, ex.Message);
            }

        }
    }
}
