﻿using Mitt.Commons.BaseModel;
using Mitt.Commons.ViewModels;
using Mitt.Data.Context;
using Mitt.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Mitt.Data.Entities;

namespace Mitt.Repository.Implementations
{
    public class SkillLevelModelAccess :ISkillLevel
    {
        private readonly MittContext _context;

        public SkillLevelModelAccess(MittContext context)
        {
            _context = context;
        }

        public async Task<ResponseDto<List<SkillLevelViewModel>>> GetAll()
        {
            try
            {
                List<SkillLevelViewModel> data = await (from a in _context.SkillLevels
                                                              select new SkillLevelViewModel
                                                              {
                                                                  SkillLevelId = a.SkillLevelId,
                                                                  SkillLevelName = a.SkillLevelName,
                                                                  CreatedBy = a.CreatedBy,
                                                                  CreatedOn = a.CreatedOn,
                                                                  ModifiedBy = a.ModifiedBy,
                                                                  ModifiedOn = a.ModifiedOn,
                                                                  DeletedBy = a.DeletedBy,
                                                                  DeletedOn = a.DeletedOn,
                                                                 
                                                              }).ToListAsync();

                return new ResponseDto<List<SkillLevelViewModel>>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<List<SkillLevelViewModel>>(false, ex.Message);
            }
        }

        public async Task<ResponseDto<SkillLevelViewModel>> GetById(int id)
        {
            try
            {
                SkillLevelViewModel data = await (from a in _context.SkillLevels
                                                        where a.SkillLevelId == id
                                                        select new SkillLevelViewModel
                                                        {
                                                            SkillLevelId = a.SkillLevelId,
                                                            SkillLevelName = a.SkillLevelName,
                                                            CreatedBy = a.CreatedBy,
                                                            CreatedOn = a.CreatedOn,
                                                            ModifiedBy = a.ModifiedBy,
                                                            ModifiedOn = a.ModifiedOn,
                                                            DeletedBy = a.DeletedBy,
                                                            DeletedOn = a.DeletedOn,
                                                        }).FirstOrDefaultAsync();
                return new ResponseDto<SkillLevelViewModel>(data);
            }
            catch (Exception ex)
            {
                return new ResponseDto<SkillLevelViewModel>(false, ex.Message);
            }
        }
        public async Task<ResponseDto<SkillLevelViewModel>> PostCreate(SkillLevelViewModel data)
        {
            try
            {
                SkillLevel a = new SkillLevel
                {
                    SkillLevelId = data.SkillLevelId,
                    SkillLevelName = data.SkillLevelName,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = data.ModifiedBy,
                    ModifiedOn = data.ModifiedOn,
                    DeletedBy = data.DeletedBy,
                    DeletedOn = data.DeletedOn,
                    
                };
                _context.SkillLevels.Add(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillLevelViewModel>();

            }
            catch (Exception ex)
            {

                return new ResponseDto<SkillLevelViewModel>(false, ex.Message);
            }

        }

        public async Task<ResponseDto<SkillLevelViewModel>> PostEdit(SkillLevelViewModel data)
        {
            try
            {
                SkillLevel a = new SkillLevel
                {
                    SkillLevelId = data.SkillLevelId,
                    SkillLevelName = data.SkillLevelName,
                    CreatedBy = data.CreatedBy,
                    CreatedOn = data.CreatedOn,
                    ModifiedBy = data.ModifiedBy,
                    ModifiedOn = data.ModifiedOn,
                    DeletedBy = data.DeletedBy,
                    DeletedOn = data.DeletedOn,
                    
                };
                _context.Update(a);
                await _context.SaveChangesAsync();
                return new ResponseDto<SkillLevelViewModel>();

            }
            catch (Exception ex)
            {

                return new ResponseDto<SkillLevelViewModel>(false, ex.Message);
            }
        }
    }
}
