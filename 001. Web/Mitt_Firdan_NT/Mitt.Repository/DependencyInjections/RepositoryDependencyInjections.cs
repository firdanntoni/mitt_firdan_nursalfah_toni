﻿using Microsoft.Extensions.DependencyInjection;
using Mitt.Repository.Implementations;
using Mitt.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mitt.Repository.DependencyInjections
{
    public static class RepositoryDependencyInjections
    {
        public static IServiceCollection AddRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<IUserProfile, UserProfileModelAccess>();
            services.AddTransient<ISkillModelAccess, SkillModelAccess>();
            services.AddTransient<ISkillLevel, SkillLevelModelAccess>();
            return services;
        }
    }
}
