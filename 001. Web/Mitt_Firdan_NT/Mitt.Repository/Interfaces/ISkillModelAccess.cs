﻿using Mitt.Commons.BaseModel;
using Mitt.Commons.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mitt.Repository.Interfaces
{
    public interface ISkillModelAccess
    {
        Task<ResponseDto<List<SkillViewModel>>> GetAll();

        Task<ResponseDto<SkillViewModel>> GetById(int id);

        Task<ResponseDto<SkillViewModel>> PostCreate(SkillViewModel data);

        Task<ResponseDto<SkillViewModel>> PostEdit(SkillViewModel data);
        Task<ResponseDto<SkillViewModel>> PostDelete(SkillViewModel data);
    }
}
