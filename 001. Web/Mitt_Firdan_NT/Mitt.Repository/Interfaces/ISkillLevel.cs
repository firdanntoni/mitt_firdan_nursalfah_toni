﻿using Mitt.Commons.BaseModel;
using Mitt.Commons.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mitt.Repository.Interfaces
{
    public interface ISkillLevel
    {
        Task<ResponseDto<List<SkillLevelViewModel>>> GetAll();
        Task<ResponseDto<SkillLevelViewModel>> GetById(int id);
        Task<ResponseDto<SkillLevelViewModel>> PostCreate(SkillLevelViewModel data);
        Task<ResponseDto<SkillLevelViewModel>> PostEdit(SkillLevelViewModel data);
    }
}
