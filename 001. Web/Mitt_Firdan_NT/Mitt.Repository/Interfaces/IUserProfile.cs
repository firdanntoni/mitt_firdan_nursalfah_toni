﻿using Mitt.Commons.BaseModel;
using Mitt.Commons.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mitt.Repository.Interfaces
{
    public interface IUserProfile
    {
        Task<ResponseDto<List<UserProfileViewModel>>> GetAll();
        Task<ResponseDto<UserProfileViewModel>> GetById(int id);
        Task<ResponseDto<UserProfileViewModel>> PostCreate(UserProfileViewModel data);
    }
}
