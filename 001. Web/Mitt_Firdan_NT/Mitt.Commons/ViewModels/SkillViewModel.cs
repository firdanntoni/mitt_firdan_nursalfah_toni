﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mitt.Commons.ViewModels
{
    public class SkillViewModel
    {
        [DisplayName("SkillId")]
        public int SkillId { get; set; }
        [DisplayName("SkillName")]
        public string SkillName { get; set; }
        [DisplayName("CreatedBy")]
        public long CreatedBy { get; set; }
        [DisplayName("CreatedOn")]
        public DateTime CreatedOn { get; set; }
        [DisplayName("ModifieBy")]
        public long? ModifiedBy { get; set; }
        [DisplayName("ModifiedOn")]
        public DateTime? ModifiedOn { get; set; }
        [DisplayName("DeletedBy")]
        public long? DeletedBy { get; set; }
        [DisplayName("DeletedOn")]
        public DateTime? DeletedOn { get; set; }
        [DisplayName("IsDelete")]
        public bool IsDelete { get; set; }
    }
}
