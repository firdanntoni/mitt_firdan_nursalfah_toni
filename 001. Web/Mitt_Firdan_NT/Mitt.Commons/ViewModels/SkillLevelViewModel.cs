﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mitt.Commons.ViewModels
{
    public class SkillLevelViewModel
    {
        [DisplayName("Skill Level Id")]
        public int SkillLevelId { get; set; }
        [DisplayName("Skill Level Name")]
        public string SkillLevelName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
