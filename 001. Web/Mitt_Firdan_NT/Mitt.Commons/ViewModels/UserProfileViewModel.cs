﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Mitt.Commons.ViewModels
{
   public class UserProfileViewModel
    {
        [DisplayName("Username")]
        public string Username { get; set; }
        [DisplayName("Id User Profile")]
        public int IdUserProfile { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        [DisplayName("Bod")]
        public DateTime Bod { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
