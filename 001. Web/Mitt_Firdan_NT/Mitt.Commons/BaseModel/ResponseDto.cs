﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mitt.Commons.BaseModel
{
    public class ResponseDto<T>
    {
        public ResponseDto()
        {
            IsSuccess = true;
            Message = string.Empty;
        }
        public ResponseDto(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            Message = message;
        }
        public ResponseDto(T data)
        {
            IsSuccess = true;
            Message = string.Empty;
            Data = data;
        }
        public ResponseDto(bool isSuccess, string message, T data)
        {
            IsSuccess = isSuccess;
            Message = message;
            Data = data;
        }

        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
