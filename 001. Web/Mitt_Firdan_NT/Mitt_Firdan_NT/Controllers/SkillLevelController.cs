﻿using Microsoft.AspNetCore.Mvc;
using Mitt.Commons.ViewModels;
using Mitt.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mitt_Firdan_NT.Controllers
{
    public class SkillLevelController : Controller
    {
        private readonly ISkillLevel _respository;

        public SkillLevelController(ISkillLevel repository)
        {
            _respository = repository;
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SkillLevelViewModel a, int id)
        {

            await _respository.PostCreate(a);

            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Index()
        {
            var data = await _respository.GetAll();

            List<SkillLevelViewModel> list = new List<SkillLevelViewModel>();

            if (data.IsSuccess)
            {
                foreach (var item in data.Data)
                {
                    list.Add(item);
                }
            }

            return View(list);
        }

        public async Task<IActionResult> Edit(int id)
        {
            var data = await _respository.GetById(id);

            SkillLevelViewModel a = new SkillLevelViewModel();
            if (data.IsSuccess && data.Data != null)
            {
                a.SkillLevelId = data.Data.SkillLevelId;
                a.SkillLevelName = data.Data.SkillLevelName;
                a.CreatedBy = data.Data.CreatedBy;
                a.CreatedOn = data.Data.CreatedOn;
                a.ModifiedBy = data.Data.ModifiedBy;
                a.ModifiedOn = data.Data.ModifiedOn;
                a.DeletedBy = data.Data.DeletedBy;
                a.DeletedOn = data.Data.DeletedOn;
                
            };
            return View(a);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SkillLevelViewModel a, int id)
        {
            var data2 = await _respository.GetById(id);
            a.CreatedOn = data2.Data.CreatedOn;
            a.CreatedBy = data2.Data.CreatedBy;
            a.ModifiedBy = 1;
            a.ModifiedOn = DateTime.Now;
            await _respository.PostEdit(a);
            return RedirectToAction("Index");
        }

    }
}
