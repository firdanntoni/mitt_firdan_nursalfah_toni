﻿using Microsoft.AspNetCore.Mvc;
using Mitt.Commons.ViewModels;
using Mitt.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mitt_Firdan_NT.Controllers
{
    public class SkillController : Controller
    {
        private readonly ISkillModelAccess _respository;

        public SkillController(ISkillModelAccess repository)
        {
            _respository = repository;
        }

        public async Task<IActionResult> Index(string search)
        {
            var data = await _respository.GetAll();

            List<SkillViewModel> list = new List<SkillViewModel>();

            if (data.IsSuccess)
            {
                if (search != null)
                {
                    foreach (var item in data.Data)
                    {
                        if (item.IsDelete == false && item.SkillName.ToLower().Contains(search.ToLower()))
                        {
                            list.Add(item);
                        }
                    }
                }
                else
                {
                    foreach (var item in data.Data)
                    {
                        if (item.IsDelete == false)
                        {
                            list.Add(item);
                        }
                    }
                }
            }

            return View(list);
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SkillViewModel a, int id)
        {

            await _respository.PostCreate(a);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(int id)
        {
            var data = await _respository.GetById(id);

            SkillViewModel a = new SkillViewModel();
            if (data.IsSuccess && data.Data != null)
            {
                a.SkillId = id;
                a.SkillName = data.Data.SkillName;
                a.CreatedBy = data.Data.CreatedBy;
                a.CreatedOn = data.Data.CreatedOn;
                a.ModifiedBy = data.Data.ModifiedBy;
                a.ModifiedOn = data.Data.ModifiedOn;
                a.IsDelete = data.Data.IsDelete;
                a.DeletedBy = data.Data.DeletedBy;
                a.DeletedOn = data.Data.DeletedOn;


            };
            return View(a);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SkillViewModel a, int id)
        {
            var data2 = await _respository.GetById(id);
            a.CreatedOn = data2.Data.CreatedOn;
            a.CreatedBy = data2.Data.CreatedBy;
            a.ModifiedBy = 1;
            a.ModifiedOn = DateTime.Now;
            await _respository.PostEdit(a);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Delete(int id)
        {
            var response = await _respository.GetById(id);

            SkillViewModel a = new SkillViewModel();
            if (response.IsSuccess && response.Data != null)
            {
                a.SkillId = id;
                a.SkillName = response.Data.SkillName;
                a.CreatedBy = response.Data.CreatedBy;
                a.CreatedOn = response.Data.CreatedOn;
                a.ModifiedBy = response.Data.ModifiedBy;
                a.ModifiedOn = response.Data.ModifiedOn;
                a.DeletedBy = response.Data.DeletedBy;
                a.DeletedOn = response.Data.DeletedOn;
                a.IsDelete = response.Data.IsDelete;
            }
            return View(a);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(SkillViewModel a, int id)
        {
            var data2 = await _respository.GetById(id);

            a.SkillName = data2.Data.SkillName;
            a.CreatedOn = data2.Data.CreatedOn;
            a.CreatedBy = data2.Data.CreatedBy;
            a.ModifiedBy = data2.Data.ModifiedBy;
            a.ModifiedOn = data2.Data.ModifiedOn;
            a.DeletedBy = 1;
            a.DeletedOn = DateTime.Now;
            a.IsDelete = true;

            await _respository.PostEdit(a);
            return RedirectToAction("Index");
        }
    }
}
