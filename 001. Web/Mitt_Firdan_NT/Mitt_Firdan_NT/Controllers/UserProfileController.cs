﻿using Microsoft.AspNetCore.Mvc;
using Mitt.Commons.ViewModels;
using Mitt.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mitt_Firdan_NT.Controllers
{
    public class UserProfileController : Controller
    {
        private readonly IUserProfile _respository;

        public UserProfileController(IUserProfile repository)
        {
            _respository = repository;
        }

        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserProfileViewModel a, int id)
        {

            await _respository.PostCreate(a);

            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Index(string search)
        {
            var data = await _respository.GetAll();

            List<UserProfileViewModel> list = new List<UserProfileViewModel>();

            if (data.IsSuccess)
            {
                if (search != null)
                {
                    foreach (var item in data.Data)
                    {
                        if (item.IsDelete == false && item.Name.ToLower().Contains(search.ToLower()))
                        {
                            list.Add(item);
                        }
                    }
                }
                else
                {
                    foreach (var item in data.Data)
                    {
                        if (item.IsDelete == false)
                        {
                            list.Add(item);
                        }
                    }
                }
            }
            return View(list);
        }
    }
}
