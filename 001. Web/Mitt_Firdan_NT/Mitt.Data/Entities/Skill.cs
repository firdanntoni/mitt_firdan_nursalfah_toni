﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Mitt.Data.Entities
{
    public partial class Skill
    {
        public int SkillId { get; set; }
        public string SkillName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
