﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Mitt.Data.Entities
{
    public partial class SkillLevel
    {
        public int SkillLevelId { get; set; }
        public string SkillLevelName { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
