﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Mitt.Data.Entities
{
    public partial class UserProfile
    {
        public string Username { get; set; }
        public int IdUserProfile { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime Bod { get; set; }
        public string Email { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
