CREATE DATABASE db_mitt_skill


CREATE TABLE db_mitt_skill.dbo.UserSkill (
	userskillID nvarchar(50) NOT NULL,
	username nvarchar(50) NOT NULL,
	skillID int NOT NULL,
	skillLevelID int NOT NULL,
	created_by bigint NOT NULL,
	created_on datetime NOT NULL,
	modified_by bigint NULL,
	modified_on datetime NULL,
	deleted_by bigint NULL,
	deleted_on datetime NULL,
	is_delete bit DEFAULT 0 NOT NULL
	CONSTRAINT UserSkills PRIMARY KEY (userskillID)
);

CREATE TABLE db_mitt_skill.dbo.skill_levels (     
skillLevelID int IDENTITY(1,1) NOT NULL,     
skillLevelName varchar(500) NULL,
created_by bigint NOT NULL,
	created_on datetime NOT NULL,
	modified_by bigint NULL,
	modified_on datetime NULL,
	deleted_by bigint NULL,
	deleted_on datetime NULL
CONSTRAINT skill_level PRIMARY KEY (skillLevelID) 
);

CREATE TABLE db_mitt_skill.dbo.[User] (
	username nvarchar(50) NOT NULL,
	password nvarchar(50) NOT NULL,
	created_by bigint NOT NULL,
	created_on datetime NOT NULL,
	modified_by bigint NULL,
	modified_on datetime NULL,
	deleted_by bigint NULL,
	deleted_on datetime NULL,
	is_delete bit DEFAULT 0 NOT NULL
	CONSTRAINT User_PK PRIMARY KEY (username)
);

CREATE TABLE db_mitt_skill.dbo.UserProfile (
	username nvarchar(50) NOT NULL,
	Id_user_profile int IDENTITY(1,1) NOT NULL,
	name varchar(50) NOT NULL,
	address nvarchar(500) NOT NULL,
	bod date NOT NULL,
	email nvarchar(50) NULL,
	created_by bigint NOT NULL,
	created_on datetime NOT NULL,
	modified_by bigint NULL,
	modified_on datetime NULL,
	deleted_by bigint NULL,
	deleted_on datetime NULL,
	is_delete bit DEFAULT 0 NOT NULL
	CONSTRAINT UserProfile_PK PRIMARY KEY (Id_user_profile)
);

CREATE TABLE db_mitt_skill.dbo.skill (
	skill_id int IDENTITY(1,1) NOT NULL,
	skill_name nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	created_by bigint NOT NULL,
	created_on datetime NOT NULL,
	modified_by bigint NULL,
	modified_on datetime NULL,
	deleted_by bigint NULL,
	deleted_on datetime NULL,
	is_delete bit DEFAULT 0 NOT NULL
	CONSTRAINT PK__tbl_m_sk__FBBA8379EA0A8EB7 PRIMARY KEY (skill_id)
);
